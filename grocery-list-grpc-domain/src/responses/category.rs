pub struct Category 
{
    pub id: i32, 
    pub name: String,
    pub active: bool
}