use std::env;
use tonic::transport::Channel;
use grocery_list_grpc_domain::responses::measurement_response::Measurement;

use crate::grocery::measurement_client::MeasurementClient;
use crate::grocery::{ GetAllMeasurementsRequest, MeasurementModel };
use crate::mappers::map_grpc_to_measurement_view_model;

pub async fn run() -> Result<Vec<Measurement>, Box<dyn std::error::Error>> {
    let uri = env::var("GROCERY_DATA_URL").expect("GROCERY_DATA_URL must be set.");
    let mut client: MeasurementClient<Channel> = MeasurementClient::connect(uri).await?;
    let request = tonic::Request::new(GetAllMeasurementsRequest {});
    let response = client.get_all_measurements(request).await?; 
    let measurements: Vec<MeasurementModel> = response.into_inner().measurements;
    
    let view_models: Vec<Measurement> = measurements.into_iter().map(map_grpc_to_measurement_view_model::map).collect();
    Ok(view_models)
}



