SELECT glim.id AS "item_id", gli."name" AS "item_name", m."name" AS "measurement_name", m.id AS "measurement_id", ms."name" AS "measurement_standard_name", 
ms.id AS "measurement_standard_id", c."name" AS "category_name", c.id AS "category_id", glim.measurement_amount AS "amount" FROM grocery_list_items_mapping glim
INNER JOIN grocery_list_item gli  ON gli.id = glim.grocery_list_id 
INNER JOIN category c ON c.id = gli.category_id  
LEFT JOIN measurement m ON m.id = glim.measurement_id 
INNER JOIN measurement_standard ms ON ms.id = m.standard_id
WHERE glim.id = $1;