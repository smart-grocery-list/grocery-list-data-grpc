use std::env;

use grocery_list_grpc_domain::responses::standard_response::Standard;
use tonic::transport::Channel;

use crate::{grocery::{standard_client::StandardClient, GetAllStandardsRequest}, mappers::map_grpc_to_standard_view_model};

pub async fn run() -> Result<Vec<Standard>, Box<dyn std::error::Error>> 
{
    
    let uri: String = env::var("GROCERY_DATA_URL").expect("GROCERY_DATA_URL must be set.");
    let mut client: StandardClient<Channel> = StandardClient::connect(uri).await?;
    let request = tonic::Request::new(GetAllStandardsRequest{});
    let response = client.get_all_standards(request).await?;
    let standards = response.into_inner().standards;


    let view_models = standards.into_iter().map(map_grpc_to_standard_view_model::map).collect();
    Ok(view_models)
}