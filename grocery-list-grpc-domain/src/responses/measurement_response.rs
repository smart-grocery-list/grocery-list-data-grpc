use super::standard_response::Standard;
pub struct Measurement
{
    pub id: i32, 
    pub name: String, 
    pub standard: Standard
}