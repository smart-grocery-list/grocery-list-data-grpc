use std::sync::Arc;

use grocery_list_grpc_domain::responses::{measurement_response::Measurement, standard_response::Standard};
use sqlx::PgPool;

#[derive(Debug)]
pub struct MeasurementRepo 
{
    pool: Arc<PgPool>
}

impl MeasurementRepo {
    pub fn new(pool: PgPool) -> Self 
    {
        Self 
        {
            pool: Arc::new(pool)
        }
    }

    pub async fn get_by_id(&self, id: i32) -> Result<Measurement, &'static str>
    {
        let measurement = sqlx::query_file!("src/scripts/GetMeasurementById.sql", id).fetch_optional(&*self.pool).await;

        match measurement {
            Ok(meas) => 
            {
                let res = meas.map(|row| {
                    Measurement 
                    {
                        id: row.measurement_id,
                        name: row.measurement_name.to_string(),
                        standard: Standard 
                        { 
                            id: row.standard_id, 
                            name: row.standard_name.to_string()
                        }
                    }
                });
        
                match res {
                    Some(meas) => 
                    {
                        Ok(meas)
                    },
                    None => 
                    {
                        Ok(Measurement { id: 0, name: "".to_string(), standard: Standard { id: 0, name: "".to_string() } })
                    }
                }
            }, 
            Err(_err) =>
            {
                Err("Failed to get measuremtent.")
            }
        }
        
    }
    
    pub async fn get_all(&self) -> Result<Vec<Measurement>, &'static str>
    {
        let measurements = sqlx::query_file!("src/scripts/GetAllMeasurements.sql").fetch_all(&*self.pool).await;

        match measurements {
            Ok(meas) => 
            {
                let results: Vec<Measurement> = meas.into_iter().map(|row| Measurement
                { 
                    id: row.measurement_id, 
                    name: row.measurement_name.to_string(), 
                    standard: Standard 
                    { 
                        id: row.standard_id, 
                        name: row.standard_name.to_string() 
                    }
                }).collect();

             Ok(results)
            }
            Err(_err) => 
            {
                Err("Failed to get all measurements.")
            }
        }
    }
}