use grocery::category_server::CategoryServer;
use grocery::grocery_list_server::GroceryListServer;
use grocery_list_data::repo::category_repo::CategoryRepo;
use grocery_list_data::repo::grocery_list_item_repo::GroceryListItemRepo;
use grocery_list_data::repo::grocery_list_repo::GroceryListRepo;
use grocery_list_data::repo::measurement_repo::MeasurementRepo;
use grocery_list_data::repo::measurement_standard_repo::MeasurementStandardRepo;
use services::category_service::CategoryService;
use sqlx::postgres::PgPoolOptions;
use sqlx::PgPool;
use std::{env, net::SocketAddr};

use dotenvy::dotenv;
use tonic::transport::Server;
use grocery::measurement_server::MeasurementServer;
use grocery::standard_server::StandardServer;

use crate::grocery::grocery_list_item_server::GroceryListItemServer;
use crate::services::grocery_list_service::GroceryListService;
use crate::services::grocery_list_item_service::GroceryListItemService;
use crate::services::measurement_service::MeasurementService;
use crate::services::standard_service::StandardService;

mod services;
mod mappers;

pub mod grocery 
{
    tonic::include_proto!("grocery");
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>>{
    dotenv().ok();
    
    let db_url = env::var("DATABASE_URL").expect("Error : Database URL must be set.");
    let min_connection: u32 = env::var("DATABASE_MIN_CONNECTION").expect("Error : Database min connections must be set.").parse::<u32>()?;
    let max_connection: u32 = env::var("DATABASE_MAX_CONNECTION").expect("Error : Datase max connections must be set.").parse::<u32>()?;

    let pool: PgPool = PgPoolOptions::new()
                        .min_connections(min_connection)
                        .max_connections(max_connection)
                        .connect(&db_url)
                        .await
                        .expect("Error while building Postgresql pool.");
    let address: SocketAddr = env::var("BASE_URL").expect("BASE_URL must be set.").parse().unwrap();

    //Create repos 
    let category_repo: CategoryRepo = CategoryRepo::new(pool.clone());
    let standard_repo: MeasurementStandardRepo = MeasurementStandardRepo::new(pool.clone());
    let measurement_repo: MeasurementRepo = MeasurementRepo::new(pool.clone());
    let grocery_list_repo: GroceryListRepo = GroceryListRepo::new(pool.clone());
    let grocery_list_item_repo: GroceryListItemRepo = GroceryListItemRepo::new(pool.clone());

    // Create services
    let category_service: CategoryService = services::category_service::CategoryService::new(category_repo);
    let grocery_list_service: GroceryListService = services::grocery_list_service::GroceryListService::new(grocery_list_repo);
    let grocery_list_item_service: GroceryListItemService = services::grocery_list_item_service::GroceryListItemService::new(grocery_list_item_repo);
    let measurement_service: MeasurementService = services::measurement_service::MeasurementService::new(measurement_repo); 
    let standard_service: StandardService = services::standard_service::StandardService::new(standard_repo);

    println!("Grocery server listening on {}", address);

    Server::builder()
        .add_service(CategoryServer::new(category_service))
        .add_service(GroceryListServer::new(grocery_list_service))
        .add_service(GroceryListItemServer::new(grocery_list_item_service))
        .add_service(MeasurementServer::new(measurement_service))
        .add_service(StandardServer::new(standard_service))
        .serve(address)
        .await?; 

    Ok(())
}
