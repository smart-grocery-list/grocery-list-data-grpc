pub struct GroceryListItem {
    pub name: String,
    pub category_id: i32
}