use crate::responses::{grocery_list_item_response::GroceryListItem, user::User};

pub struct GroceryList {
    pub name: String,
    pub items: Vec<GroceryListItem>, 
    pub users: Vec<User>
}