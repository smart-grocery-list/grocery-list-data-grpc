use std::env;
use tonic::transport::Channel;

use grocery_list_grpc_domain::responses::measurement_response::Measurement; 
use crate::grocery::measurement_client::MeasurementClient;
use crate::grocery::{ GetMeasurementByIdRequest, MeasurementModel, };
use crate::mappers::map_grpc_to_measurement_view_model;

pub async fn run(measurement_id: i32) -> Result<Measurement, Box<dyn std::error::Error>> 
{
    let uri: String = env::var("GROCERY_DATA_URL").expect("GROCERY_DATA_URL must be set.");
    let mut client: MeasurementClient<Channel> = MeasurementClient::connect(uri).await?;
    let request: tonic::Request<GetMeasurementByIdRequest> = tonic::Request::new(GetMeasurementByIdRequest { id: measurement_id});
    let response: tonic::Response<crate::grocery::MeasurementModel> = client.get_measurement(request).await?;
    let measurement: MeasurementModel = response.into_inner();

    let view_model: Measurement = map_grpc_to_measurement_view_model::map(measurement);
    Ok(view_model)
}