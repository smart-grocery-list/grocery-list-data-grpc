use std::env;

use grocery_list_grpc_domain::responses::category::Category;
use tonic::transport::Channel;

use crate::{grocery::{category_client::CategoryClient, CategoryModel, GetCategoryByIdRequest}, mappers::map_grpc_to_category_view_model};

pub async fn run(category_id: i32) -> Result<Category, Box<dyn std::error::Error>> 
{
    let uri: String = env::var("GROCERY_DATA_URL").expect("GROCERY_DATA_URL must be set.");
    let mut client: CategoryClient<Channel> = CategoryClient::connect(uri).await?;
    let request = tonic::Request::new(GetCategoryByIdRequest { id: category_id });
    let response = client.get_category(request).await?;
    let category: CategoryModel = response.into_inner();
    
    let view_models: Category = map_grpc_to_category_view_model::map(category);

    Ok(view_models)
}