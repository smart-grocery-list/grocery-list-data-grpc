use grocery_list_grpc_domain::responses::measurement_response::Measurement;
use crate::grocery::MeasurementModel;
use crate::grocery::StandardModel;

pub fn map(measurement: Measurement) -> MeasurementModel {
    let standard_model: Option<StandardModel> = Some(StandardModel { standard_id: measurement.standard.id, standard_name: measurement.standard.name });
    MeasurementModel { measurement_id: measurement.id, measurement_name: measurement.name, standard: standard_model }
}