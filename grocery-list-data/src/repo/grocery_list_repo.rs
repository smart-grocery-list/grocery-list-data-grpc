use std::sync::Arc;

use grocery_list_grpc_domain::responses::grocery_list::GroceryList;
use sqlx::PgPool;

#[derive(Debug)]
pub struct GroceryListRepo 
{
    pool: Arc<PgPool>
}

impl GroceryListRepo 
{
    pub fn new(pool: PgPool) -> Self
    {
        Self 
        {
            pool: Arc::new(pool)
        }
    }

    pub async fn get_by_id(&self, id: i32) -> Result<GroceryList, &'static str>
    {
        let grocery_list = sqlx::query_file_as!(GroceryList, "src/scripts/GetGroceryListById.sql", id).fetch_optional(&*self.pool).await;

        match grocery_list 
        {
            Ok(Some(list)) => 
            {
                Ok(list)
            },
            Ok(None) => 
            {
                Ok(GroceryList { id: 0, name: "".to_string(), complete_date: None })
            },
            _ =>
            {
                Err("Failed to get grocery list.")
            }
        }
    }

    pub async fn get_all_by_user_id(&self, user_id: i32) -> Result<Vec<GroceryList>, &'static str>
    {
        let grocery_lists = sqlx::query_file_as!(GroceryList, "src/scripts/GetAllGroceryListsByUserId.sql", user_id).fetch_all(&*self.pool).await;

        match grocery_lists 
        {
            Ok(lists) => 
            {
                Ok(lists)
            }
            _ =>
            {
                Err("Failed to get all grocery lists.")
            }

        }
    }

    pub async fn create(&self, item: grocery_list_grpc_domain::requests::create_grocery_list_request::GroceryList) -> Result<i32, &'static str>
    {
        let res = sqlx::query_file!("src/scripts/CreateNewGroceryList.sql", item.name).fetch_one(&*self.pool).await;
    
        match res {
            Ok(res) => 
            {
                Ok(res.id)
            },
            _ => {
                Err("Failed to create grocery list.")
            }
        }
    }
    
    pub async fn complete_list(&self, id: i32) -> Result<bool, &'static str>
    {
        let res = sqlx::query_file!("src/scripts/CompleteGroceryList.sql", id).execute(&*self.pool).await;

        match res 
        {
            Ok(res) => 
            {
                Ok(res.rows_affected() == 1)
            }, 
            _ => 
            {
                Err("Failed to complete grocery list.")
            }
        }
    }


    pub async fn update_list(&self, id: i32, item: grocery_list_grpc_domain::requests::create_grocery_list_request::GroceryList) -> Result<bool, &'static str>
    {
        let res = sqlx::query_file!("src/scripts/UpdateGroceryList.sql", item.name, id).execute(&*self.pool).await; 

        match res 
        {
            Ok(res) => 
            {
                Ok(res.rows_affected() > 0)
            },
            _ => 
            {
                Err("Failed to update grocery list.")
            }
        }
    }

    pub async fn add_user_to_list(&self, list_id: i32, user_id: i32, is_primary: bool) -> Result<bool, &'static str>
    {
        let res = sqlx::query_file!("src/scripts/CreateNewGroceryListUserMapping.sql", list_id, user_id, is_primary).execute(&*self.pool).await;
        match res 
        {
            Ok(res) => 
            {
                Ok(res.rows_affected() > 0)
            },
            _ => 
            {
                Err("Failed to add user to grocery list.")
            }
        }
    }

    pub async fn remove_user_to_list(&self, list_id: i32, user_id: i32) -> Result<bool, &'static str> 
    {
        let res = sqlx::query_file!("src/scripts/DeleteGroceryListMapping.sql", list_id, user_id).execute(&*self.pool).await; 
        
        match res 
        {
            Ok(res) => 
            {
                Ok(res.rows_affected() > 0)
            },
            _ => 
            {
                Err("Failed to remove user from grocery list.")
            }
        }
    }

    pub async fn delete(&self, id: i32) -> Result<bool, &'static str>
    {
        let res = sqlx::query_file!("src/scripts/DeleteGroceryList.sql", id).execute(&*self.pool).await; 

        match res 
        {
            Ok(res) => 
            {
                Ok(res.rows_affected() > 0)
            },
            _ => 
            {
                Err("Failed to delete grocery list.")
            }
        }
    }
}
