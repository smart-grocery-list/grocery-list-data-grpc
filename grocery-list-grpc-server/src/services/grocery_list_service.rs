use grocery_list_data::repo::grocery_list_repo::GroceryListRepo;
use tonic::{Response, Request, Status};

use crate::{grocery::{grocery_list_server::GroceryList, GetAllGroceryListsByUserRequest, GetAllGroceryListsByUserResponse, GetGroceryListByIdRequest, GroceryListModel, UpdateGroceryListRequest, UpdateGroceryListResponse, CompleteGroceryListRequest, CompleteGroceryListResponse,  CreateGroceryListResponse, DeleteGroceryListRequest, DeleteGroceryListResponse, CreateGroceryListRequest}, mappers::{domain_to_grocery_list_response, proto_to_grocery_list}};

#[derive(Debug)]
pub struct GroceryListService 
{
    repo: GroceryListRepo
}

impl GroceryListService 
{
    pub fn new(repo: GroceryListRepo) -> Self
    {
        Self 
        { 
            repo 
        }
    }
}


#[tonic::async_trait]
impl GroceryList for GroceryListService 
{
    async fn complete_grocery_list(&self, request: Request<CompleteGroceryListRequest>) -> Result<Response<CompleteGroceryListResponse>, Status> 
    {
        let req = request.into_inner();
        let result = self.repo.complete_list(req.id).await;

        match result 
        {
            Ok(res) => 
            {
                Ok(Response::new(CompleteGroceryListResponse { is_completed: res }) )
            },
            _ => 
            {
                Err(Status::new(tonic::Code::Internal, "Failed to complete grocery list."))
            }
        }
    }

    async fn create_grocery_list(&self, request: Request<CreateGroceryListRequest>) -> Result<Response<CreateGroceryListResponse>, Status>
    {
        let req = request.into_inner();    
        let list = match req.list
        {
            Some(list) =>
            {
                list 
            },
            None =>
            {
                panic!("No grocery list data provided")
            }
        };
        let grocery_list = proto_to_grocery_list::map(list);
        let result = self.repo.create(grocery_list).await;

        match result 
        {
            Ok(res) => 
            {
                Ok(Response::new(CreateGroceryListResponse { id: res }))
            },
            _ =>
            {
                Err(Status::new(tonic::Code::Internal, "Failed to create a new grocery list."))
            }
        }
    }

    async fn delete_grocery_list(&self, request: Request<DeleteGroceryListRequest>) -> Result<Response<DeleteGroceryListResponse>, Status>
    {
        let req = request.into_inner(); 
        let result = self.repo.delete(req.id).await;

        match result 
        {
            Ok(result) =>
            {
                if result
                {
                    Ok(Response::new(DeleteGroceryListResponse { is_deleted: result }))
                }
                else 
                {
                    Err(Status::new(tonic::Code::Internal, "Failed to delete grocery list."))
                }
            },
            _ => 
            {
                Err(Status::new(tonic::Code::Internal, "Failed to delete grocery list."))
            }
        }
    }

    async fn get_all_grocery_lists_by_user(&self, request: Request<GetAllGroceryListsByUserRequest>) -> Result<Response<GetAllGroceryListsByUserResponse>, Status>
    {
        let req = request.into_inner();
        let result = self.repo.get_all_by_user_id(req.user_id).await;

        match result 
        {
            Ok(res) => 
            {

               Ok(Response::new(GetAllGroceryListsByUserResponse 
                { 
                    lists : res.into_iter().map(domain_to_grocery_list_response::map).collect()
                }))

            },
            _ => 
            {
                Err(Status::new(tonic::Code::Internal, "Failed to get all grocery list associated with user."))
            }
        }
    }

    async fn get_grocery_list_by_id(&self, request: Request<GetGroceryListByIdRequest>) -> Result<Response<GroceryListModel>, Status>
    {
        let req = request.into_inner(); 

        let result = self.repo.get_by_id(req.id).await;

        match result 
        {
            Ok(res) => 
            {
                
                let date: String = match res.complete_date 
                {
                    Some(date) => date.to_string(), 
                    None => "".to_string()
                };

                Ok(Response::new(GroceryListModel { id: res.id, name: res.name, complete_date:  date }))
            },
            _ => 
            {
                Err(Status::new(tonic::Code::Internal, "Failed to get grocery list."))
            }
        }
    }

    async fn update_grocery_list(&self, request: Request<UpdateGroceryListRequest>) -> Result<Response<UpdateGroceryListResponse>, Status>
    {
        let req = request.into_inner();
        let id = req.id; 
        let list = match req.grocery_list
        {
            Some(list) =>
            {
                proto_to_grocery_list::map(list)
            },
            None =>
            {
                panic!("No grocery list data provided")
            }
        };
        let result = self.repo.update_list(id, list).await;
        
        match result 
        {
            Ok(res) => 
            {
                Ok(Response::new(UpdateGroceryListResponse { is_updated: res }))
            },
            _ => 
            {
                Err(Status::new(tonic::Code::Internal, "Failed to update grocery list."))
            }
        }
    }
}