use super::measurement_response::Measurement;
use super::category::Category;
pub struct GroceryListItem 
{
    pub id: i32, 
    pub name: String,
    pub measurement: Measurement,
    pub measurement_ammount: f64,
    pub category: Category,
}