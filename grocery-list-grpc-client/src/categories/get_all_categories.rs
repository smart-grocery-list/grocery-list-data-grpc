use std::env;

use grocery_list_grpc_domain::responses::category::Category;
use tonic::transport::Channel;

use crate::{grocery::{category_client::CategoryClient, GetAllCategoriesRequest, CategoryModel}, mappers::map_grpc_to_category_view_model};


pub async fn run() -> Result<Vec<Category>, Box<dyn std::error::Error>> 
{
    let uri: String = env::var("GROCERY_DATA_URL").expect("GROCERY_DATA_URL must be set.");
    let mut client: CategoryClient<Channel> = CategoryClient::connect(uri).await?;
    let request = tonic::Request::new(GetAllCategoriesRequest {});
    let response = client.get_all_categories(request).await?;
    let categories: Vec<CategoryModel> = response.into_inner().categories;
    
    let view_models: Vec<Category> = categories.into_iter().map(map_grpc_to_category_view_model::map).collect();

    Ok(view_models)
}
