use grocery_list_data::repo::measurement_repo::MeasurementRepo;
use tonic::{ Request, Response, Status };
use crate::grocery::measurement_server::Measurement;
use crate::grocery::{ 
    GetAllMeasurementsRequest, 
    GetAllMeasurementsResponse, 
    GetMeasurementByIdRequest, 
    MeasurementModel
};
use crate::mappers::domain_to_measurement_responce;

#[derive(Debug)]
pub struct MeasurementService {
    repo: MeasurementRepo
}

impl MeasurementService {
    pub fn new(repo: MeasurementRepo) -> Self
    {
        Self 
        {
            repo
        }
    }
}

#[tonic::async_trait]
impl Measurement for MeasurementService 
{
    async fn get_measurement(&self, request: Request<GetMeasurementByIdRequest>) -> Result<Response<MeasurementModel>, Status>
    {
        let req = request.into_inner();

        let result = self.repo.get_by_id(req.id).await;

        match result {
            Ok(res) => {
                Ok(Response::new(domain_to_measurement_responce::map(res)))
            }
            _ => {
                Err(Status::new(tonic::Code::Internal, "Failed to get measurement."))
            }
        }
    }

    async fn get_all_measurements(&self, _request: Request<GetAllMeasurementsRequest>) -> Result<Response<GetAllMeasurementsResponse>, Status>
    {

        let result = self.repo.get_all().await; 

        match result {
            Ok(res) => {
               Ok(Response::new(GetAllMeasurementsResponse {
                    measurements: res.into_iter().map(domain_to_measurement_responce::map).collect()
               }))
            },
            _ => {
                Err(Status::new(tonic::Code::Internal, "Failed to get all measurements."))
            }
        }
    }
}

