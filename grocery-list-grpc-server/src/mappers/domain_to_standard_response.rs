use grocery_list_grpc_domain::responses::standard_response::Standard;

use crate::grocery::StandardModel;

pub fn map(standard: Standard) -> StandardModel 
{
    StandardModel { standard_id: standard.id, standard_name: standard.name }
}