use chrono::{DateTime, Utc};

pub struct GroceryList 
{
    pub id: i32, 
    pub name: String, 
    pub complete_date: Option<DateTime<Utc>>,
}