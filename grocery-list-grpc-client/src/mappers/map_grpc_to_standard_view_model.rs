use grocery_list_grpc_domain::responses::standard_response::Standard;

use crate::grocery::StandardModel;


pub fn map(response: StandardModel) -> Standard 
{
    Standard { id: response.standard_id, name: response.standard_name }
}