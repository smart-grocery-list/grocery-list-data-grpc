use grocery_list_grpc_domain::responses::category::Category;

use crate::grocery::CategoryModel;

pub fn map(response: CategoryModel) -> Category
{
    Category { id: response.category_id, name: response.category_name, active: true }
}