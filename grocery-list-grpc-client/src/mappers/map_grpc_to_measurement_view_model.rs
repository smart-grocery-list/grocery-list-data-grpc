use grocery_list_grpc_domain::responses::{measurement_response::Measurement, standard_response::Standard};

use crate::grocery::MeasurementModel;

pub fn map(response: MeasurementModel) -> Measurement {
   
   let standard = match response.standard {
        Some(res) => {
            res
        },
        None => {
            panic!("No standard found for measurement")
        }
    };

    Measurement { id: response.measurement_id, name: response.measurement_name, standard: Standard { id: standard.standard_id, name: standard.standard_name }  }
}