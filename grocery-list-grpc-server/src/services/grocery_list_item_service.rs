use grocery_list_data::repo::grocery_list_item_repo::GroceryListItemRepo;
use tonic::{Response, Request, Status};

use crate::{grocery::{grocery_list_item_server::GroceryListItem, GetAllItemsByListRequest, GetAllItemsByListResponse, GetItemByIdRequest, GetItemByIdResponse, CreateItemRequest, CreateItemResponse}, mappers::domain_to_grocery_list_item_response};

#[derive(Debug)]
pub struct GroceryListItemService 
{
    repo: GroceryListItemRepo
}

impl GroceryListItemService 
{
    pub fn new(repo: GroceryListItemRepo) -> Self
    {
        Self
        {
            repo
        }
    }
}

#[tonic::async_trait]
impl GroceryListItem for GroceryListItemService 
{
    async fn get_all_items_by_list(&self, request: Request<GetAllItemsByListRequest>) -> Result<Response<GetAllItemsByListResponse>, Status>
    {
        let req = request.into_inner(); 
        let result = self.repo.get_all_items_by_list_id(req.id).await;   
        
        match result 
        {
            Ok(res) => 
            {
                Ok(Response::new(GetAllItemsByListResponse 
                    { 
                        item: res.into_iter().map(domain_to_grocery_list_item_response::map).collect()
                    }
                ))
            },
            _ => 
            {
                Err(Status::new(tonic::Code::Internal, "Failed to get items."))
            }
        }
    }
    
    async fn get_item_by_id(&self, request: Request<GetItemByIdRequest>) -> Result<Response<GetItemByIdResponse>, Status>
    {
        let req = request.into_inner();
        let result = self.repo.get_by_id(req.id).await;

        match result 
        {
            Ok(res) => 
            {
                Ok(Response::new(GetItemByIdResponse { item: Some(domain_to_grocery_list_item_response::map(res)) }))
            }, 
            _ => 
            {
                Err(Status::new(tonic::Code::Internal, "Failed to get items"))
            }
        }
    }

    async fn create_item(&self, _request: Request<CreateItemRequest>) -> Result<Response<CreateItemResponse>, Status>
    {
        unimplemented!()
    }
}