use grocery_list_data::repo::category_repo::CategoryRepo;
use tonic::{Response, Request, Status};

use crate::{grocery::{category_server::Category, GetAllCategoriesRequest, GetAllCategoriesResponse, GetCategoryByIdRequest, CategoryModel}, mappers::domain_to_category_response}; 


#[derive(Debug)]
pub struct CategoryService {
    repo: CategoryRepo
}

impl CategoryService {
    pub fn new(repo: CategoryRepo) -> Self
    {
        Self 
        {
            repo
        }
    }
}

#[tonic::async_trait]
impl Category for CategoryService {
    async fn get_category(&self, request: Request<GetCategoryByIdRequest>) -> Result<Response<CategoryModel>, Status>
    {
        let req = request.into_inner(); 

        let result = self.repo.get_by_id(req.id).await;

        match result 
        {
            Ok(res) =>
            {
                Ok(Response::new(domain_to_category_response::map(res)))
            }
            _ => 
            {
                Err(Status::new(tonic::Code::Internal, "Failed to get category."))
            }
        }
    }
    async fn get_all_categories(&self, _request: Request<GetAllCategoriesRequest>) -> Result<Response<GetAllCategoriesResponse>, Status>
    {
        let result = self.repo.get_all().await;
        
        match result 
        {
            Ok(res) => 
            {
                Ok(Response::new(GetAllCategoriesResponse 
                    {
                        categories: res.into_iter().map(domain_to_category_response::map).collect()
                    }
                ))
            },
            _ => 
            {
                Err(Status::new(tonic::Code::Internal, "Failed to get category."))
            }
        }
    }
}