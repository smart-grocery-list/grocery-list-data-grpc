SELECT m.id as measurement_id, m.name as measurement_name, ms.id as standard_id, ms.name as standard_name FROM public.measurement m 
INNER JOIN public.measurement_standard ms ON m.standard_id = ms.id
WHERE m.id = $1;