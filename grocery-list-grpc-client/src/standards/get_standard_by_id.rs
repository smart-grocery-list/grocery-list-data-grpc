use std::env;

use grocery_list_grpc_domain::responses::standard_response::Standard;
use tonic::transport::Channel;

use crate::{grocery::{GetStandardByIdRequest, standard_client::StandardClient}, mappers::map_grpc_to_standard_view_model};


pub async fn run(standard_id: i32) -> Result<Standard, Box<dyn std::error::Error>>
{
    let uri: String = env::var("GROCERY_DATA_URL").expect("GROCERY_DATA_URL must be set.");
    let mut client: StandardClient<Channel> = StandardClient::connect(uri).await?;
    let request = tonic::Request::new(GetStandardByIdRequest
    {
        id: standard_id
    });
    let response = client.get_standard(request).await?;
    let standard = response.into_inner();

    let view_models = map_grpc_to_standard_view_model::map(standard);
    Ok(view_models)
}