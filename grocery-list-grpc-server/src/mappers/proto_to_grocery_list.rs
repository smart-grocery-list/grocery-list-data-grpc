use grocery_list_grpc_domain::requests::create_grocery_list_request::GroceryList;

use crate::grocery::CreateGroceryListModel;

pub fn map (grocery_list: CreateGroceryListModel) -> GroceryList 
{
    GroceryList { name: grocery_list.name, items: Vec::new(), users: Vec::new() }
}