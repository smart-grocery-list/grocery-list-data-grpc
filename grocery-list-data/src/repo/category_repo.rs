use std::sync::Arc;

use grocery_list_grpc_domain::responses::category::Category;
use sqlx::PgPool;

#[derive(Debug)]
pub struct CategoryRepo
{
    pool: Arc<PgPool>
}

impl CategoryRepo 
{

    pub fn new(pool: PgPool) -> Self
    {
        Self 
        { 
            pool: Arc::new(pool)
        }
    }

    pub async fn get_by_id(&self, id: i32) -> Result<Category, &'static str>
    {
        let category: Result<Option<Category>, sqlx::Error> = sqlx::query_file_as!(Category, "src/scripts/GetCategoryById.sql", id).fetch_optional(&*self.pool).await;

        match category {
            Ok(Some(cat)) => 
            {
                Ok(cat)
            },
            Ok(None) => 
            {
                Ok(Category { id: 0, name: "".to_string(), active: false })
            },
            Err(_err) =>
            {
                Err("Failed to get category.")
            }
        }
    }

    pub async fn get_all(&self) -> Result<Vec<Category>, &'static str>
    {
        let categories: Result<Vec<Category>, sqlx::Error> = sqlx::query_file_as!(Category, "src/scripts/GetAllCategories.sql").fetch_all(&*self.pool).await;
        
        match categories {
            Ok(cat) => 
            {
                Ok(cat)
            },
            Err(_err) => 
            {
                Err("Failed to get all categories")
            }
        }
    }
}
