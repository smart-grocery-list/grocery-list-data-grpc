SELECT gl.id AS id, gl.name AS name, gl.complete_date at time zone 'UTC' AS complete_date
FROM public.grocery_list gl 
INNER JOIN public.grocery_list_user_mapping usrm on usrm.grocery_list_id = gl.id
WHERE usrm.grocery_list_user_id = $1;