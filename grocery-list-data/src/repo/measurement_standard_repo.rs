use std::sync::Arc;

use grocery_list_grpc_domain::responses::standard_response::Standard;
use sqlx::PgPool;

#[derive(Debug)]
pub struct MeasurementStandardRepo
{
    pool: Arc<PgPool>
}


impl MeasurementStandardRepo {
    
    pub fn new(pool: PgPool) -> Self
    {
        Self
        { 
            pool: Arc::new(pool)
        }
    }

    pub async fn get_by_id(&self, id: i32) -> Result<Standard, &'static str> 
    {
        let standard: Result<Option<Standard>, sqlx::Error> = sqlx::query_file_as!(Standard, "src/scripts/GetMeasurementStandardById.sql", id).fetch_optional(&*self.pool).await;

        match standard 
        {
            Ok(Some(std)) => 
            {
                Ok(std)
            },
            Ok(None) => 
            {
                Ok(Standard { id: 0, name: "".to_string() })
            }
            Err(_err) =>
            {
                Err("Failed to load standard.")
            }
        }
    }

    pub async fn get_all(&self) -> Result<Vec<Standard>, &'static str>
    {
        let standards: Result<Vec<Standard>, sqlx::Error> = sqlx::query_file_as!(Standard, "src/scripts/GetAllMeasurementStandards.sql").fetch_all(&*self.pool).await;
        
        match standards 
        {
            Ok(std) => 
            {
                println!("Total results : {}", std.len());
                Ok(std)
            },
            Err(err) => 
            {
                println!("Error : {}", err);
                Err("Failed to get all standards. ")
            }
        }
    }
}