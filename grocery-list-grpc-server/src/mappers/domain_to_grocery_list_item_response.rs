use grocery_list_grpc_domain::responses::grocery_list_item_response::GroceryListItem;

use crate::grocery::{MeasurementModel, CategoryModel, ItemModel};

use super::{domain_to_measurement_responce, domain_to_category_response};

pub fn map(item: GroceryListItem) -> ItemModel
{
    let category: CategoryModel = domain_to_category_response::map(item.category);
    let measurement: MeasurementModel = domain_to_measurement_responce::map(item.measurement);
    ItemModel { id: item.id, name: item.name, measurement_ammount: item.measurement_ammount, measurement: Some(measurement), category: Some(category) }
}