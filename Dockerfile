FROM rust:1.69.0 AS builder

WORKDIR /usr/src/Grocery-List-Data-Grpc 

COPY . .

RUN apt update && apt upgrade -y
RUN apt install -y protobuf-compiler libprotobuf-dev


RUN cargo install --path grocery-list-grpc-server/
FROM debian:bullseye-slim 
RUN apt-get update && rm -rf /var/lib/apt/lists/*

COPY --from=builder /usr/local/cargo/bin/grocery-list-grpc-server /usr/local/bin/grocery-list-grpc-server

EXPOSE 5051

CMD ["grocery-list-grpc-server"]
