use std::sync::Arc;

use grocery_list_grpc_domain::{responses::{grocery_list_item_response::GroceryListItem, measurement_response::Measurement, standard_response::Standard, category::Category}, requests::create_item_request};
use sqlx::PgPool;

#[derive(Debug)]
pub struct GroceryListItemRepo
{
    pool: Arc<PgPool>
}

impl GroceryListItemRepo
{
    pub fn new(pool: PgPool) -> Self
    {
        Self 
        {
            pool: Arc::new(pool)
        }
    }

    pub async fn get_by_id(&self, id: i32) -> Result<GroceryListItem, &'static str>
    {
        let res = sqlx::query_file!("src/scripts/GetItemById.sql", id).fetch_optional(&*self.pool).await;

        match res 
        {
            Ok(res) =>
            {
                let result = res.map(|row| {
                    GroceryListItem
                    {
                        id: row.item_id,
                        name: row.item_name,
                        measurement_ammount: row.amount,
                        measurement: Measurement
                        {
                            id: row.measurement_id,
                            name: row.measurement_name, 
                            standard: Standard 
                            {
                                id: row.measurement_standard_id,
                                name: row.measurement_standard_name
                            }
                        },
                        category: Category 
                        {
                            id: row.category_id,
                            name: row.category_name, 
                            active: true
                        }
                    }
                });

                match result 
                {
                    Some(item) =>
                    {
                        Ok(item)
                    }, 
                    None => 
                    {
                        let item =  GroceryListItem
                        {
                            id: 0,
                            name: "".to_string(),
                            measurement_ammount: 0.0,
                            measurement: Measurement
                            {
                                id: 0,
                                name: "".to_string(), 
                                standard: Standard 
                                {
                                    id: 0,
                                    name: "".to_string()
                                }
                            },
                            category: Category 
                            {
                                id: 0,
                                name: "".to_string(), 
                                active: false
                            }
                        };

                        Ok(item)
                    }

                }
            }, 
            _ => 
            {
                Err("Failed to get item.")
            }
        }
    }

    pub async fn get_all_items_by_list_id(&self, id: i32) -> Result<Vec<GroceryListItem>, &'static str>
    {
        let res = sqlx::query_file!("src/scripts/GetAllItemsByListId.sql", id).fetch_all(&*self.pool).await; 
        match res 
        {
            Ok(res) =>
            {
                let result = res.into_iter().map(|row| {
                    GroceryListItem
                    {
                        id: row.item_id,
                        name: row.item_name,
                        measurement_ammount: row.amount,
                        measurement: Measurement
                         {
                            id: row.measurement_id,
                            name: row.measurement_name, 
                            standard: Standard 
                            {
                                id: row.measurement_standard_id,
                                name: row.measurement_standard_name
                            }
                        },
                        category: Category 
                        {
                            id: row.category_id,
                            name: row.category_name, 
                            active: true
                        }
                    }
                }).collect();

                Ok(result)
            },
            _ => 
            {
                Err("Failed to get items for list")
            }
        }
    }

   pub async fn create_item(&self, list: create_item_request::GroceryListItem) -> Result<i32, &'static str>
   {
        let name: String = list.name.trim().to_ascii_lowercase();
        let res = sqlx::query_file!("src/scripts/CreateNewItem.sql", name, list.category_id).fetch_one(&*self.pool).await;

        match res 
        {
            Ok(res) => 
            {
                Ok(res.id)
            },
            _ => 
            {
                Err("Failed to create new item.")
            }
        }
   }
}