use grocery_list_data::repo::measurement_standard_repo::MeasurementStandardRepo;
use tonic::{Request, Response, Status};

use crate::{grocery::{standard_server::Standard, GetStandardByIdRequest, StandardModel, GetAllStandardsRequest, GetAllStandardsResponse}, mappers::domain_to_standard_response};

#[derive(Debug)]
pub struct StandardService {
    repo: MeasurementStandardRepo
}

impl StandardService {
    pub fn new(repo: MeasurementStandardRepo) -> Self
    {
        Self 
        {
            repo
        }
    }
}

#[tonic::async_trait]
impl Standard for StandardService 
{
    async fn get_standard(&self, request: Request<GetStandardByIdRequest>) -> Result<Response<StandardModel>, Status>
    {
        let request: GetStandardByIdRequest = request.into_inner(); 
        let id: i32 = request.id; 
        let result = self.repo.get_by_id(id).await;

        match result 
        {
            Ok(res) =>
            {
                Ok(Response::new(domain_to_standard_response::map(res)))
            }, 
            Err(err) => {
                println!("Failed to get standard (grpc) {}", err);
                Err(Status::new(tonic::Code::Internal, "Failed to get standard."))
            }
        }
    }

    async fn get_all_standards(&self, _request: Request<GetAllStandardsRequest>) -> Result<Response<GetAllStandardsResponse>, Status>
    {
        let result = self.repo.get_all().await;

        match result 
        {
            Ok(res) => 
            {
                Ok(Response::new(GetAllStandardsResponse { 
                    standards: res.into_iter().map(domain_to_standard_response::map).collect()
                }))
            },
            Err(err) => {
                println!("Failed to get all standards (grpc) {}", err);
                Err(Status::new(tonic::Code::Internal, "Failed to get all standards."))
            }
        } 
    }
}