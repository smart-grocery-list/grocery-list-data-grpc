use grocery_list_grpc_domain::responses::grocery_list::GroceryList;

use crate::grocery::GroceryListModel;


pub fn map(grocery_list: GroceryList) -> GroceryListModel
{
    let date: i64 = 0; 
    GroceryListModel { id: grocery_list.id, name: grocery_list.name, complete_date: date.to_string() }
}