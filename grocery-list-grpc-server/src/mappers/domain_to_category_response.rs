use grocery_list_grpc_domain::responses::category::Category;

use crate::grocery::CategoryModel;


pub fn map(category: Category) -> CategoryModel 
{
    CategoryModel { category_id: category.id, category_name: category.name }
}